// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class SNAKE_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	// Top-down camera
	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	// Reference to the controllable snake actor.
	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	// Reference to the class (not the object!) for actor spawn.
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Spawns snake.
	void CreateSnakeActor();

	// Player input handlers.
	UFUNCTION()
	void OnVerticalInput(float value);
	UFUNCTION()
	void OnHorizontalInput(float value);

};
