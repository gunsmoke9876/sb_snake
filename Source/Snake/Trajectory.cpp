// © GunSmoke, 2022


#include "Trajectory.h"

	//Get location and rotation in horizontal plane at the given distance along the way.
	//If distance exceeds segment's length, delegate this function to the next segment with reduced Length argument.
	//Always returns scale {1, 1, 1}.
	//Always returns Z = 0.0, Roll = 0.0, Pitch = 0.0.
FTransform WaySegment::GetPointAt(float Length)
{
	//Declare variables to return. For straight way by default.
	FVector ReturnLocation = BeginLocation + BeginRotation.Vector() * Length;
	FRotator ReturnRotation = BeginRotation;

	//Apply horizontal turn.
	if (RHor != 0.0) // > 0 - right turn, < 0 - left turn
	{
		float AngleHor = Length / RHor; //angle of turn [radians]
		ReturnRotation.Yaw += AngleHor * 57.296;

		//Finding ReturnLocation is a problem of rotate one point relatively another by the given angle.
		//First, calc rotation of start point. Then add rotation by given angle.
		float XCenter = BeginLocation.X - RHor * sin(BeginRotation.Yaw / 57.296);
		float YCenter = BeginLocation.Y + RHor * cos(BeginRotation.Yaw / 57.296);

		ReturnLocation.X = XCenter + RHor * sin(BeginRotation.Yaw / 57.296 + AngleHor);
		ReturnLocation.Y = YCenter - RHor * cos(BeginRotation.Yaw / 57.296 + AngleHor);
	}

	return { ReturnLocation, ReturnRotation , {1.0, 1.0, 1.0} };
}


Trajectory::Trajectory()
{
}

Trajectory::~Trajectory()
{
}

