// © GunSmoke, 2022

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

class SNAKE_API WaySegment
{
private:
	FVector BeginLocation; //World location of segment start.
	FRotator BeginRotation; //Start direction of segment.
	float RHor = 0.0; //Radius of segment. 
				//R == 0.0 means infinite radius - straight line;
	            //R > 0.0 means right turn (for trajectory; for truck it's left turn, cause it moves towards trajectory);
	            //R < 0.0 means left turn.
	float L; //Lenght of segment. At that lenght the next segment begins.
	WaySegment* Next; //Pointer to the next segment.
public:
	WaySegment();

	//Get location and rotation in horizontal plane at the given distance along the way.
	//If distance exceeds segment's length, delegate this function to the next segment with reduced Length argument.
	//Always returns scale {1, 1, 1}.
	//Always returns Z = 0.0, Roll = 0.0, Pitch = 0.0.
	FTransform GetPointAt(float Length);

	//Note: All the vertical bend and pitch and roll must be applied later on, depending on the prop.
};

class SNAKE_API Trajectory
{
private:
	WaySegment* FrontSegment; //first way segment, where the tractor goes.
public:
	Trajectory();
	~Trajectory();
};
