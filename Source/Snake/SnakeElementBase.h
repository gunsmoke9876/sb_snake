// © GunSmoke, 2022

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;

UCLASS()
class SNAKE_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	//Owner of this element.
	ASnakeBase* SnakeOwner;

	// Element mesh.
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Interaction with another actor handler
	UFUNCTION()
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	//Overlap handler.
	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		                    AActor* OtherActor,
		                    UPrimitiveComponent* OtherComp,
		                    int32 OtherBodyIndex,
		                    bool bFromSweep,
		                    const FHitResult& SweepResult);

	//Switch on and off element's mesh collision. In order not to collide with self while moving.
	UFUNCTION()
	void ToggleCollision();

};
