// � GunSmoke, 2022


#include "PlayerPawnBase.h"
#include "SnakeBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create camera subobject
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();

	// Aim camera top-down
	SetActorRotation(FRotator(-90.0, 0.0, 0.0));
	
	// Spawn snake.
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::OnVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::OnHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	//SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(ASnakeBase::StaticClass(), FTransform());
	//SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform()); // TODO: FIX TRANSFORM
	
	FVector SnakeStartLocation = GetActorLocation(); // This should be location of PlayerStart object on the level.
	SnakeStartLocation.Z = 50.0; // Snake starts at this height, cm.
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(SnakeStartLocation));

}

// Player input handlers.
void APlayerPawnBase::OnVerticalInput(float value)
{
	if (!SnakeActor) return;

	if (value > 0.0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		SnakeActor->CurrentMoveDirection = EMovementDirection::UP;
	else if (value < 0.0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		SnakeActor->CurrentMoveDirection = EMovementDirection::DOWN;
}
void APlayerPawnBase::OnHorizontalInput(float value)
{
	if (!SnakeActor) return;

	if (value > 0.0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		SnakeActor->CurrentMoveDirection = EMovementDirection::RIGHT;
	else if (value < 0.0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		SnakeActor->CurrentMoveDirection = EMovementDirection::LEFT;

}