// � GunSmoke, 2022


#include "SnakeBase.h"
#include "Interactable.h"
#include "SnakeElementBase.h"
//#include "Materials/MaterialInterface.h" //for setting material of snake's head

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.0f;
	MovementTimeStep = .75f;
	LastMoveDirection = EMovementDirection::UP;
	CurrentMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementTimeStep);
	AddSnakeElement(4); // Create this much elements.
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int el = 0; el < ElementsNum; el++)
	{
		FTransform NewTransform = FTransform(GetActorLocation() + FVector(SnakeElements.Num() * ElementSize * -1, 0.0, 0.0));

		// This can spawn actor of the class, specified in blueprint.
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		// Attaching to parent actor may be useful in most cases. But we do our way.
		//NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		SnakeElements.Add(NewSnakeElem);
		NewSnakeElem->SnakeOwner = this;

		if (SnakeElements.Num() == 1)
			// HeadMaterial needs to be set in BP_Snake blueprint.
			NewSnakeElem->MeshComponent->SetMaterial(0, HeadMaterial);
	}
}

// Move at current movement direction. DeltaTime is in [s].
void ASnakeBase::Move()
{
	FVector MovementVector;
	switch (CurrentMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector = FVector(1.0f, 0.0f, 0.0f);
		break;
	case EMovementDirection::DOWN:
		MovementVector = FVector(-1.0f, 0.0f, 0.0f);
		break;
	case EMovementDirection::RIGHT:
		MovementVector = FVector(0.0f, 1.0f, 0.0f);
		break;
	case EMovementDirection::LEFT:
		MovementVector = FVector(0.0f, -1.0f, 0.0f);
		break;
	}
	MovementVector *= ElementSize;

	//AddActorWorldOffset(MovementVector); // Moves whole snake, but elements are no longer attached to it.

	SnakeElements[0]->ToggleCollision(); //head's collision off
	
	// Moving all the elements separately, beginning from tail. Each element takes place of previous element.
	for (int el = SnakeElements.Num() - 1; el > 0; el--)
	{
		SnakeElements[el]->SetActorLocation(SnakeElements[el - 1]->GetActorLocation());
	}

	// Moving the snake's head.
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	
	SnakeElements[0]->ToggleCollision(); //head's collision on

	// Update snake's last movement direction.
	LastMoveDirection = CurrentMoveDirection;

}
//One of snake's element reports, that it has overlapped something.
void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		//Find OverlappedElement among SnakeElements array.
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		//Determine, whether the element is snake's head.
		bool bIsHead = ElemIndex == 0;
		//Get interactable interface from other actor being overlapped by OverlappedElement.
		IInteractable* InteractableInterface = nullptr;
		InteractableInterface = Cast<IInteractable>(Other);
		//Try to interact with ovelapped actor.
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsHead);
		}
	}
}
