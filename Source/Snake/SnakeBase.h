// � GunSmoke, 2022

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP, RIGHT, DOWN, LEFT
};

UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	// Period of performing movement, [s].
	UPROPERTY(EditDefaultsOnly)
	float MovementTimeStep;

	// Elements of snake.
	UPROPERTY() 
	TArray<ASnakeElementBase*> SnakeElements;

	// Material of snake's head is set in blueprint.
	UPROPERTY(EditAnywhere)
	UMaterialInterface* HeadMaterial;

	// Direction of previous movement.
	UPROPERTY()
	EMovementDirection LastMoveDirection;

	// Direction of current (expecting) movement.
	UPROPERTY()
	EMovementDirection CurrentMoveDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Fill SnakeElements array.
	void AddSnakeElement(int ElementsNum = 1);

	// Move at last movement direction. DeltaTime is in [s].
	void Move();

	//One of snake's element reports, that it has overlapped something.
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

};
