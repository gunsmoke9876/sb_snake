// © GunSmoke, 2022

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "PickUp.generated.h"

UCLASS()
class SNAKE_API APickUp : public AActor, public IInteractable 
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Interaction with another actor handler
	UFUNCTION()
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
